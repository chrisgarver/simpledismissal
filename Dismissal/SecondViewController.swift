//
//  SecondViewController.swift
//  Dismissal
//
//  Created by Chris Garver on 8/28/15.
//  Copyright © 2015 Bottle Rocket Studios. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBAction func cancelPressed(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
